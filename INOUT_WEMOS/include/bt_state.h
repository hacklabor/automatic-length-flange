#pragma once
#include <Arduino.h>


enum BtState
    {
        BtNone,
        BtClick,
        BtHold
      

    };


class bt_state
{
private:
    


    int pinBt;
    long lastReleaseTime = 0;
    long HoldTime = 0;
    BtState BtS = BtNone;


public:
    bt_state(int pin,long HoldTime);
    BtState check();
};
