#pragma once
#include <Arduino.h>
#include "LedControl.h"
#include "SerialDebug.h"
#include "bt_state.h"

enum Status
{
    _statMenuAktive,
    _statDigitNR,
    _statSoll,
    _statIst
};


class display
{
private:
    


    int TimeOUT = 0;
    
    
    int maxMenu = 12;
    int digitNr = 4;
    signed int sollWert[12] = {}; //manu + P0-9
    int istWert = 0;
    int menu = 0;
    boolean blink;
    Status stat = _statMenuAktive;





    void showInteger(LedControl &disp,int val,boolean blink);

public:
    display();
    void refreshDisplay(LedControl &disp, SerialDebug &dbg); // Händeln was auf dem Display angezeigt wird
   
    
    int encoderPlus(SerialDebug &dbg);
    int encoderMinus(SerialDebug &dbg);
    void setSollWert(int value, int pos);
    void setStatus(Status _stat);
    int getSollWert(int pos);
    void toggleBlink();

};
