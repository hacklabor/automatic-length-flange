#include <Arduino.h>

//We always have to include the library
#include "LedControl.h"
#include "TimeTrigger.h"
#include "display.h"

#include "bt_state.h"

/*
 Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
 pin 12 (D7) is connected to the DataIn 
 pin 11 (D5) is connected to the CLK 
 pin 10 (D4) is connected to LOAD 
 We have only a single MAX72XX.
 */
LedControl lc=LedControl(D7,D5,D4,1);
display disp=display();
SerialDebug deb = SerialDebug(ALL);
TimeTrigger _400ms = TimeTrigger(600);
TimeTrigger _2000ms = TimeTrigger(2000);
bt_state But(D6, 500);


/* we always wait a bit between updates of the display */

int pinRelais = D1;
int pinEnc1 = D2;
int pinEnc2 = D3;

 int pinALast;
 int aVal;
 boolean trig = false;
 boolean bCW;
 BtState BtS = BtNone;
 long lastBtTime = 0;

 BtState lastBtS = BtNone;
 void test()
 {

   return;
}

void setup() {

  Serial.begin(115200);
  /*
   The MAX72XX is in power-saving mode on startup,
   we have to do a wakeup call
   */

  pinMode(pinRelais, OUTPUT);
  pinMode(pinEnc1, INPUT_PULLUP);
  pinMode(pinEnc2, INPUT_PULLUP);
  
   pinALast = digitalRead(pinEnc1); 

  pinMode(pinRelais, OUTPUT);
  pinMode(pinRelais, OUTPUT);

  digitalWrite(pinRelais, HIGH);
  lc.shutdown(0,false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0,8);
  /* and clear the display */
  lc.clearDisplay(0);

//attachInterrupt(digitalPinToInterrupt(pinEnc1),test, RISING);
 


}

void loop() {

  BtS = But.check();
  if (lastBtS != BtS){
     deb.print("Button", String(BtS), DEBUG);
    if (lastBtS == BtHold){
       disp.setStatus(_statSoll);

    }

  

    lastBtS = BtS;
    
  }
  if (BtS == BtHold)
    {
    disp.setStatus(_statDigitNR);
    lastBtTime = millis();
   
    }

  aVal = digitalRead(pinEnc1);
  if (aVal == HIGH)
  {
    trig = false;
    delay(5);
   }

  
 if (aVal == LOW && !trig){
   lastBtTime = millis();
   trig = true;
   pinALast = aVal;
   // Means the knob is rotating
 // if the knob is rotating, we need to determine direction
 // We do that by reading pin B.
    if (digitalRead(pinEnc2) != aVal) { // Means pin A Changed first - We're Rotating Clockwise
      bCW = true;
      }
  else {// Otherwise B changed first and we're moving CCW
 bCW = false;
 }
  if (!bCW){
      disp.encoderMinus(deb);
      disp.refreshDisplay(lc, deb);
    }
     if (bCW){
      disp.encoderPlus(deb);
      disp.refreshDisplay(lc, deb);
      
    }
    
   
 }

 
 
 if (millis()-lastBtTime>5000){
   disp.setStatus(_statMenuAktive);
 }

 if (_400ms.Trigger())
 {
   disp.toggleBlink();
   disp.refreshDisplay(lc, deb);
  }
  
   
}