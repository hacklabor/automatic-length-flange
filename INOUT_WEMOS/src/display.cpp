#include <Arduino.h>
#include "display.h"

display::display(/* args */)
{
}




void display ::refreshDisplay(LedControl &disp, SerialDebug &dbg) // Händeln was auf dem Display angezeigt wird

{
    // menue
    if (menu == 0){
         disp.setChar(0,7,'H',false);
         disp.setChar(0,6,'A',false);
         disp.setChar(0,5,' ',false);
         showInteger(disp, sollWert[0],blink);
    }
    else  if(menu < 11){

    

        disp.setChar(0,7,'P',false);
        disp.setDigit(0,6,menu-1,false);
        disp.setChar(0,5,' ',false);
        if (stat == _statMenuAktive){
            showInteger(disp, sollWert[menu-1],blink);
        }
         if (stat == _statSoll){
            disp.setChar(0, 5, 'P', false);
            showInteger(disp, sollWert[menu-1],true);
        }
        
    }
    else{
        disp.setChar(0, 7, ' ', false);
        disp.setChar(0, 6, ' ', false);
        disp.setRow(0,5,0x05);
        disp.setChar(0, 4, 'E', false);
        disp.setChar(0, 3, 'F', false);
        disp.setChar(0, 2, ' ', false);
        disp.setChar(0, 1, ' ', false);
        disp.setChar(0, 0, ' ', false);
        }

    
}




void display::showInteger(LedControl &disp,int val,boolean blink){
    
    byte dig = 0;
    int div = 10000;
    if (val > 99999)
    {
        val = 99999;
    }
    for (int i = 0; i < 5;i++){
        
         dig = val / div;
         if (blink && stat !=_statDigitNR){
             disp.setDigit(0, 4 - i, dig, (i == 3) );
         }
         else{
             if (stat!=_statDigitNR || stat==_statIst ){
                 disp.setChar(0, 4 - i, ' ', false);
             }
             else
             {
                 if (digitNr== 4-i){
                     disp.setChar(0, 4 - i, dig, (i == 3));
                 }
                 else{
                     disp.setChar(0, 4 - i, ' ', false);
                 }

             }
             
         }
        
         val = val % div;
         div /= 10;
         
    }
    
   
}

void display::setStatus(Status _stat)
{
    stat = _stat;
}

int display::encoderPlus(SerialDebug &deb){
    deb.print("status", String(stat), DEBUG);

    if (stat == _statIst){
        return 0;
    }
    
    if (stat == _statMenuAktive ){
        menu += 1;
        if (menu > maxMenu)
        {
            menu = maxMenu;
        }
        return 0;
    }

  if (stat == _statSoll){
      sollWert[menu - 1] += pow(10, digitNr);
      deb.print("Sollwert", String(    sollWert[menu - 1] ), DEBUG);
      deb.print("digitNr", String(   digitNr ), DEBUG);
      if (sollWert[menu - 1] > 99999)
      {
          sollWert[menu - 1] = 99999;
        }
        
        return 0;

    }
   
 if (stat == _statDigitNR){
        
        if (digitNr <= 0)
        {
           digitNr = 1;
        }
        digitNr -= 1;
        return 0;

    }

    return 0;
}




int display::encoderMinus(SerialDebug &deb){

    deb.print("status", String(stat), DEBUG);

    if (stat == _statMenuAktive)
    {
        if (menu <= 0)
        {
            menu = 0;
            return 0;
        }

        menu -= 1;

        return 0;
 }
 
   
     
       if (stat == _statDigitNR){
        digitNr += 1;
        if (digitNr > 4)
        {
           digitNr = 4;
        }
        return 0;
    }
     
    
    if (stat == _statSoll){
        sollWert[menu - 1] -=  pow(10, digitNr);
        if (sollWert[menu - 1] < 0)
        {
            sollWert[menu - 1] = 0;
            }
            
            return 0;

        }
        return 0;
}

    void display::setSollWert(int value, int pos){ }
    int display::getSollWert(int pos) { return 0; }


      void display:: toggleBlink(){
          if (blink){
              blink = false;
          }
          else{
              blink = true;
          }
       
      }