#include <Arduino.h>
#include "bt_state.h"

bt_state:: bt_state(int _pin,long _HoldTime)
{
     pinMode(_pin, INPUT_PULLUP);
     pinBt = _pin;
     HoldTime = _HoldTime;
}


BtState bt_state::check(){


    if (!digitalRead(pinBt) && BtS==BtNone){
    
        delay(HoldTime);
        lastReleaseTime = millis();

        if (!digitalRead(pinBt))
        {
            BtS = BtHold;
           
           
        }
        else 
        {
            BtS = BtClick;
            
        }
    }
    
    if (digitalRead(pinBt) && BtS != BtNone)
    {

        if (millis()-lastReleaseTime>10)
        {
            BtS = BtNone;
        }
    }
    else
    {
        lastReleaseTime = millis();
    }

    return BtS;

}

