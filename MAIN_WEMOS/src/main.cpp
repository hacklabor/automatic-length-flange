#include <arduino.h>
#include "LittleFS.h"
#include <FileSystem.h>
#include <SerialDebug.h>

#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <MD5Builder.h>
#include <main.h>
#include <secrets.h>
#include <ESP8266WiFiAP.h>
#include <stdlib.h>




unsigned int rxcount = 0;
int state = 0;
String rxdat = "";
String RXDAT = "";
String LastIN = "";
float backlslash = 0.5;
long oldtime = 0;
bool NewData = false;
FileSystem fileSys(DEBUG);
SerialDebug DebugMain(DEBUG);
AsyncWebServer server(80);
motorData mdata;
motorData mdataOLD;
String ret = "";
MD5Builder _md5;


IPAddress local_ip(192,168,42,1);
IPAddress gateway(192,168,42,254);
IPAddress netmask(255,255,255,0);


char buf[20];



void printParameter(){

  DebugMain.print("PosAkt", dtostrf( mdata.PosAkt, 3, 3, buf), DEBUG);
  DebugMain.print("PosNull",dtostrf( mdata.PosNull, 3, 3, buf), DEBUG);
  DebugMain.print("PosSoll",dtostrf( mdata.PosSoll, 3, 3, buf), DEBUG);
  DebugMain.print("PosMax",dtostrf( mdata.PosMax, 3, 3, buf), DEBUG);
  DebugMain.print("SpeedMax",dtostrf( mdata.maxSpeed, 3, 3, buf), DEBUG);
  DebugMain.print("Step_je_mm",dtostrf( mdata.Steps_je_mm,3, 3, buf), DEBUG);
  DebugMain.print("Beschleunigung",dtostrf( mdata.accerlation, 3, 3, buf), DEBUG);
  DebugMain.print("Umkehrspiel",dtostrf( mdata.backslash, 3, 3, buf), DEBUG);
  DebugMain.print("REF",String(mdata.ref), DEBUG);
  DebugMain.print("State",String(mdata.state), DEBUG);
 
  
}

String md5(String str) {
  _md5.begin();
  _md5.add(String(str));
  _md5.calculate();
  return _md5.toString();
}




String processor(const String &var)
{ 
  return "lerr";
}

String CheckREF(String normal){
 if (mdata.ref)
 {
   return normal;
 }
 return "/referenz.html";
}
String CreateTXString(){
   String message = "";
       // ["PaSpeed","PaAccer","PaResul","PaMax","PaBacksl"]
        message += String(mdata.maxSpeed) + "/";
        message += String(mdata.accerlation) + "/";
        message += String(mdata.Steps_je_mm) + "/";
        message += String(mdata.PosMax) + "/";
        message += String(mdata.backslash);
        return message;
}
void SERVER()
{
  // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("IndexHTML", "LOAD", DEBUG);
    
    request->send(LittleFS ,CheckREF ("/index.html"), String(), false, processor);
  });

  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("Style.css", "LOAD", DEBUG);
    request->send(LittleFS, "/style.css", "text/css");
  });
  // Rout to load sript.js file
  server.on("/script.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("script.js", "LOAD", DEBUG);
    request->send(LittleFS, "/script.js", "text/javascript");
  });
  server.on("/referenz.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("refrenz.js", "LOAD", DEBUG);
    request->send(LittleFS, "/referenz.js", "text/javascript");
  });
  server.on("/parameter.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("parameter.js", "LOAD", DEBUG);
    request->send(LittleFS, "/parameter.js", "text/javascript");
  });
  server.on("/settings.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("settings.js", "LOAD", DEBUG);
    request->send(LittleFS, "/settings.js", "text/javascript");
  });
  server.on("/favicon.ico", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("favicon.ico", "LOAD", DEBUG);
    request->send(LittleFS, "favicon.ico", "text/plain");
  });
  server.on("/parameter.html", HTTP_GET, [](AsyncWebServerRequest *request) {
    if(!request->authenticate(http_username, http_password))
    return request->requestAuthentication();
     DebugMain.print("parameter.html", "LOAD", DEBUG);
    request->send(LittleFS , "/parameter.html", String(), false);
  });
   server.on("/settings.html", HTTP_GET, [](AsyncWebServerRequest *request) {
    if(!request->authenticate(http_username, http_password))
    return request->requestAuthentication();
     DebugMain.print("settings.html", "Settings", DEBUG);
    request->send(LittleFS , "/settings.html", String(), false);
  });
   server.on("/referenz.html", HTTP_GET, [](AsyncWebServerRequest *request) {
      DebugMain.print("referenz.html", "Referenz", DEBUG);
    request->send(LittleFS , "/referenz.html", String(), false);
  });

   server.on("/aktPos", HTTP_GET, [](AsyncWebServerRequest *request)
             {
             
               String text = "";

               text += dtostrf(mdata.PosAkt + mdata.PosOffset, 3, 3, buf);
               if (mdata.ref){
                  text += "/" + String(mdata.state);
               } 
               else
               {
                 text += "/9";
               }
               text += "/" + String(mdata.PosMax);
               text += "/" + String(mdata.NextMem);

               request->send_P(200, "text/plain", text.c_str());
             });
   server.on("/RefP", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send_P(200, "text/plain", String(mdata.PosNull).c_str()); });
   server.on("/ParM", HTTP_GET, [](AsyncWebServerRequest *request)
             {
               String message = CreateTXString();
               request->send_P(200, "text/plain", message.c_str());
             });
     server.on("/savM", HTTP_GET, [](AsyncWebServerRequest *request)
             {
               String message = "";
               for (int i = 0; i < 10;i++){
                            
                    message += String(mdata.mem[i])+"/";
               
               }
               message += dtostrf(mdata.PosAkt + mdata.PosOffset, 3, 3, buf);
               message += "/";
               message += dtostrf(mdata.PosSoll, 3, 3, buf);
               request->send_P(200, "text/plain", message.c_str());
             });
   server.on(
       "/data", HTTP_POST, [](AsyncWebServerRequest *request) {}, NULL,
       [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
       { 
         String val[20];
             for (size_t i = 0; i < len; i++)
          {
            rxdat += char(data[i]);
          }

          if (index + len == total)
          {
            
            DebugMain.print("POST DATA", rxdat, DEBUG);
            int z = 0;
            int s = 0;

            while (true)
            {
            int n = rxdat.indexOf('/', s);
            if (s-1 == rxdat.lastIndexOf('/')){
            break;
             }

            
            String va = rxdat.substring(s, n);
            DebugMain.print("DATA", va, DEBUG);
            val[z] = va;
            z++;
            s = n + 1;

            }
            rxdat = "";

            if (val[0]=="RefP"){
              
              mdata.PosNull = val[1].toFloat();
           mdata.PosOffset = mdata.PosNull-mdata.PosAkt ;
          
         
           mdata.ref = true;
             request->send(200);
            }
            else{
              request->send(200);
            }
          }
        
         
         if (val[0] == "SOLL" and state==0)
         {
           if (val[2]=="abs"){
             mdata.PosSoll = val[1].toFloat();
           }
           if (val[2]=="rel"){
             mdata.PosSoll = mdata.PosAkt + mdata.PosOffset+ val[1].toFloat();
           }

          if (mdata.PosSoll<0){
            mdata.PosSoll = 0;
          } 
           if (mdata.PosSoll>mdata.PosMax){
            mdata.PosSoll = mdata.PosMax;
          } 
           
          
           
         
           if (mdata.PosSoll<=mdata.PosAkt+mdata.PosOffset){
              mdata.state = 4;
           }
           else
           {
              mdata.state = 2;
           }
           Serial.write(0x18);
                  }
         else if (val[0] == "STOP")
         {
           Serial.println("!");
         }
          else if (val[0] == "NextM")
         {
          mdata.NextMem=val[1].toFloat();
         }
         else if (val[0] == "ParM")

         {

           mdataOLD = mdata;
           String mes = "";
           int z = 1;
           mdata.maxSpeed = val[z].toFloat();
           mes += val[z] + "/";
           z++;
           mdata.accerlation = val[z].toFloat();
           mes += val[z] + "/";
           z++;
           mdata.Steps_je_mm = val[z].toFloat();
           mes += val[z] + "/";
           z++;
           mdata.PosMax = val[z].toFloat();
           mes += val[z] + "/";
           z++;
           mdata.backslash = val[z].toFloat();
           mes += val[z] + "/";
           z++;
          
           fileSys.WriteNewFile((char *)"paraM", (char *)mes.c_str());

           printParameter();
           if (mdata.Steps_je_mm!=mdataOLD.Steps_je_mm){
             String t = "$100=";
             t += String(mdata.Steps_je_mm);
             Serial.println(t);
             delay(100);
           }
            if (mdata.accerlation!=mdataOLD.accerlation){
             String t = "$120=";
             t += String(mdata.accerlation);
             Serial.println(t);
           }
         }
         else if (val[0] == "savM")

         {

         
           String mes = "";
           for (int z = 0; z < 10;z++){
             mdata.mem[z] = val[z + 1].toFloat();
              mes += val[z] + "/";
           }
           fileSys.WriteNewFile((char *)"savM", (char *)mes.c_str());
         }
         
       });

   // Start server
   
}

void readData(String TXT, float &val){

}

void setup(void)
{
  delay(2000);
  Serial.begin(115200);
 

  Serial.println();
  Serial.println();

  fileSys.mount();
  
  
  
 String TXT = fileSys.ReadFile((char *)"paraM");
 if (TXT=="notExists"){
   String mes = CreateTXString();
   mes += "/";
   fileSys.WriteNewFile((char *)"paraM", (char *)mes.c_str());
   TXT = fileSys.ReadFile((char *)"paraM");
 }

int s = 0;
float val[20];
int z = 0;

val[0] = 0;

while (true)
{
  int n = TXT.indexOf('/', s);
  if (s-1 == TXT.lastIndexOf('/')){
    break;
  }

  z++;
  String va = TXT.substring(s, n);
  DebugMain.print("DATA", va, DEBUG);
  val[z] = va.toFloat();
  s = n + 1;

 }
 z  = 1;
 mdata.maxSpeed = val[z];
 z++;
 mdata.accerlation = val[z];
 z++;
 mdata.Steps_je_mm = val[z];
 z++;
 mdata.PosMax = val[z];
 z++;
 mdata.backslash = val[z];
 z++;

 TXT = fileSys.ReadFile((char *)"savM");
 if (TXT=="notExists"){
   String mes = "1/2/3/4/-1/-1/-1/-1/-1/-1/-1/";
  
   fileSys.WriteNewFile((char *)"savM", (char *)mes.c_str());
   TXT = fileSys.ReadFile((char *)"savM");
 }

s = 0;
float val1[20];
z = 0;

val1[0] = 0;

while (true)
{
  int n = TXT.indexOf('/', s);
  if (s-1 == TXT.lastIndexOf('/')){
    break;
  }

  z++;
  String va = TXT.substring(s, n);
  DebugMain.print("DATA", va, DEBUG);
  val1[z] = va.toFloat();
  s = n + 1;

 }
 for (int i = 0; i < 10;i++){
   mdata.mem[i] = val1[i + 1];
 }

   // Wifi Config
   //WiFi.begin(SSID, PASS);
  WiFi.mode(WIFI_AP);           //Only Access point
  //Start HOTspot removing password will disable security
	Serial.print("HotSpt IP:");
	Serial.print("Setting soft-AP configuration ... ");
	Serial.println(WiFi.softAPConfig(local_ip, gateway, netmask) ? "Ready" : "Failed!");


	Serial.print("Setting soft-AP ... ");
	Serial.println(	WiFi.softAP(SSID, PASS) ? "Ready" : "Failed!");

	Serial.print("Soft-AP IP address = ");

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());
  SERVER();
  server.begin();

  printParameter();
  Serial.println("$X");
  delay(200);
  Serial.println("G92 X0");

}



void loop()

{

  
 
  
  while (Serial.available()){
    char ch = Serial.read();
   
    ret += ch;
    if (ch=='\n'){
      ret = "";
    }
    if (ch==','){
      
      
      if (ret[0]=='<'){
        String ap = "";
        ap = ret.substring(ret.lastIndexOf(":") + 1, ret.indexOf(","));
        mdata.PosAkt = ap.toFloat();
        Serial.println(ap);

        if (ret[1] == 'I' or ret[1] == 'H')
        {
          if (mdata.state > 0)
          {
            
            mdata.state--;
        }
        
      }
      ret = "";
       
      }
     
      
      ret = "";
      break;
        }
  }
    

 

  if (millis()-oldtime>900){
    oldtime = millis();
    //Serial.println(mdata.PosAkt);
     
   
       Serial.println("?");
  }
  if (mdata.state==4 and state!=4)
  {
    state = 4;

    String txt = "G1 X";
    float val = mdata.PosSoll - 0.1-mdata.PosOffset;
    txt += String(val);
    txt += " F" + String(mdata.maxSpeed);
    Serial.println(txt);
  }
if (mdata.state==3 and state!=3)
  {
    state = 3;

   String txt = "G1 X";
    float val = mdata.PosSoll-mdata.PosOffset;
    txt += String(val);
    txt += " F" + String(mdata.maxSpeed);
    Serial.println(txt);
  }
  
  if (mdata.state==2 and state!=2)
  {
    state = 2;

    String txt = "G1 X";
    float val = mdata.PosSoll + mdata.backslash-mdata.PosOffset;
    txt += String(val);
    txt += " F" + String(mdata.maxSpeed);
    Serial.println(txt);
  }
  if (mdata.state==1 and state!=1)
  {
    state = 1;
    String txt = "G1 X";
    float val = mdata.PosSoll-mdata.PosOffset;
    txt += String(val);
    txt += " F" + String(mdata.maxSpeed);
    Serial.println(txt);
  }
  if (mdata.state==0){
    state = 0;
  }

  


 
  

}