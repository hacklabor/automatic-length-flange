#pragma once
#include <Arduino.h>
#include <AccelStepper.h>


struct motorData{
  long PosAkt = 999999; // 1/100mm
  long PosNull = 0;
  long PosSoll = 999999;
  bool ref = false;
  int state = 0;
  int distanceToGo = 0;
  int PosMax = 999999;
  int maxSpeed = 5;
  int Pulse_je_umdrehung = 200;
  int Steps_je_cm = 7.5 * Pulse_je_umdrehung;
  int accerlation = 3;
  int backslash = 10;
 
  
};




class stepMotor
{
private:
    motorData motor;
    
   
    long mm100_to_pulse(long pulse);
  

public:
    stepMotor(motorData _motor);
    motorData setMotorData(motorData _motor);
    motorData getMotorData(motorData _motor);
    void setMainMotorData(AccelStepper &stepper);
    long go_to_mm100(long pos);
    motorData backslash_Comp();
    long pulse_to_mm100(long pulse);
    motorData run();
    motorData stopp();
    motorData setNull(AccelStepper &stepper, long pos);
};




