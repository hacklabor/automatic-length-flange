#pragma once
#include <arduino.h>

struct motorData{
  float PosAkt = 200; // 1/100mm
  float PosNull = 0;
  float PosOffset = 0;
  float PosSoll = 0;
  bool absrel = true;
  bool ref = false;
  int state = 0;
  float PosMax = 500;
  float maxSpeed = 600;
  float Steps_je_mm = 2400;
  float accerlation = 1;
  float backslash = 1;
  float NextMem = 0;
  float mem[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
};
